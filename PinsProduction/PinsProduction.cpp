﻿/*
* Сачков Степан, БПИ192
* УСЛОВИЕ: Задача о производстве булавок. В цехе по заточке булавок все
* необходимые операции осуществляются тремя рабочими. Первый из них
* берет булавку и проверяет ее на предмет кривизны. Если булавка не кривая,
* то рабочий передает ее своему напарнику. Напарник осуществляет
* собственно заточку и передает заточенную булавку третьему рабочему,
* который осуществляет контроль качества операции. Требуется создать
* многопоточное приложение, моделирующее работу цеха. При решении
* использовать парадигму «производитель-потребитель».
*/
#include <iostream>
#include <vector>
#include <optional>
#include <thread>
#include <mutex>
#include <chrono>
#include <random>

using namespace std;

/// <summary>
/// Структура, описывающая булавку.
/// </summary>
struct MyObject {
	long const pinNumber;

	explicit MyObject(long n) : pinNumber(n) {}
};

mutex mtx; // мутекс
long numberOfPins = 0; // количество булавок

// Объекты, которые будут хранить булавки.
// ВНИМАНИЕ: optional доступен только с C++17.
optional<MyObject> currentObj, countedObj;

/// <summary>
/// Процесс проверки булавок на кривизну.
/// </summary>
/// <param name="count">Количество булавок</param>
void checkCurvature(long count) {
	while (count > 0) {
		if (!currentObj.has_value()) {
			// Защита от владения двумя объектами сразу.
			lock_guard<mutex> lock(mtx);

			cout << "The first worker checks if the pin_" << numberOfPins - count + 1 << " is curved . . ." << endl;

			if (rand() % 2 == 0) {
				cout << "The pin is NOT curvated! The first worker passes the pin_" <<
					numberOfPins - count + 1 << " to his partner\n" << endl;

				currentObj.emplace(numberOfPins - count + 1);
			}
			else {
				cout << "The pin_" <<
					numberOfPins - count + 1 << " turned out to be curvated!" << endl;

				currentObj.emplace(-1);
			}

			count--;
		}
	}
}

/// <summary>
/// Процесс заострения булавок.
/// </summary>
/// <param name="count">Количество булавок</param>
void sharpenPins(long count) {
	while (count > 0) {
		if (currentObj.has_value() && !countedObj.has_value()) {
			// Защита от владения двумя объектами сразу.
			lock_guard<mutex> lock(mtx);

			countedObj.emplace(currentObj.value());

			if (countedObj.value().pinNumber != -1) {
				cout << "The second worker sharpen the pin_" << currentObj.value().pinNumber << " . . .\n" << endl;
			}

			currentObj.reset();

			count--;
		}
	}
}

/// <summary>
/// Процесс контроля качества.
/// </summary>
/// <param name="count">Количество булавок</param>
void controlQuality(long count) {
	while (count > 0) {
		if (countedObj.has_value()) {
			// Защита от владения двумя объектами сразу.
			lock_guard<mutex> lock(mtx);

			if (countedObj.value().pinNumber != -1) {
				cout << "The third worker controls the quality of the pin_" <<
					countedObj.value().pinNumber << " . . ." << endl;

				if (rand() % 2 == 0) {
					cout << "The pin_" <<
						countedObj.value().pinNumber << " has a GOOD quality!\n" << endl;
				}
				else {
					cout << "The pin_" <<
						countedObj.value().pinNumber << " has a BAD quality!\n" << endl;
				}
			}

			countedObj.reset();

			count--;
		}
	}
}

int main() {
	cout << "Enter the number of pins: ";
	cin >> numberOfPins;

	thread Worker1(checkCurvature, numberOfPins);
	thread Worker2(sharpenPins, numberOfPins);
	thread Worker3(controlQuality, numberOfPins);

	Worker1.detach();
	Worker2.detach();
	Worker3.join();

	return 0;
}
